var searchData=
[
  ['s_121',['s',['../class_estado.html#a4463e5840c51c0c56d4da72b34c78739',1,'Estado']]],
  ['screenheight_122',['screenheight',['../structtext__info.html#a4443bbcfceb654c604f61e598167dc28',1,'text_info']]],
  ['screenwidth_123',['screenwidth',['../structtext__info.html#a9288a421d0440fd73b7ec71692d84ca6',1,'text_info']]],
  ['setano_124',['setAno',['../class_data_hora.html#ac449b4cfe574ad0ce34266710be4a037',1,'DataHora']]],
  ['setdata_125',['setData',['../class_data_hora.html#a053c6c1a76e988121df2a39b170f3c55',1,'DataHora']]],
  ['setdatahora_126',['setDataHora',['../class_estado.html#a2c6189d0817052ab20c4207cb904d077',1,'Estado']]],
  ['setdia_127',['setDia',['../class_data_hora.html#a81cdd10e549da54a76532aa28f54fce8',1,'DataHora']]],
  ['setmes_128',['setMes',['../class_data_hora.html#ac808466f9534d5ab5bea822f7019f31f',1,'DataHora']]],
  ['setres_129',['setRes',['../class_estado.html#a44bdf876d194b011884de7a52d0811f3',1,'Estado']]],
  ['sets_130',['setS',['../class_estado.html#a91eaea7e0d767c10460b597914185fbe',1,'Estado']]],
  ['settemperatura_131',['setTemperatura',['../class_estado.html#ad551449dcf226ad6083595846b7020e0',1,'Estado']]],
  ['setvent_132',['setVent',['../class_estado.html#a0eefabe4068be7ea855330346bbccc43',1,'Estado']]],
  ['stopbits_133',['Stopbits',['../temperatura_8h.html#afb371079af6706a4ccfbec4f4607747c',1,'temperatura.h']]]
];
