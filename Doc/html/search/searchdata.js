var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstvwy",
  1: "deilpt",
  2: "cdehlt",
  3: "acdefgilmoprsw",
  4: "_acnpstv",
  5: "bcps",
  6: "_bcdeglmortwy",
  7: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "related"
};

var indexSectionLabels =
{
  0: "Todos",
  1: "Classes",
  2: "Arquivos",
  3: "Funções",
  4: "Variáveis",
  5: "Enumerações",
  6: "Enumeradores",
  7: "Amigas"
};

