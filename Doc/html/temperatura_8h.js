var temperatura_8h =
[
    [ "portaSerial", "classporta_serial.html", "classporta_serial" ],
    [ "Baudrate", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5", [
      [ "B50", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a62132e262b9618d411a73de7af566616", null ],
      [ "B110", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5ae00c48d75e718531de43ed20a3ac71c0", null ],
      [ "B150", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5acbe4f54a4bd5d8019147d4a218cb6797", null ],
      [ "B300", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a4e0e62df12a632a143f9971eaf04e42c", null ],
      [ "B1200", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a0ba4cbcc6b1389b02b4b113ce7b4825e", null ],
      [ "B2400", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5add0bdd744763c5b9475683fdc3855276", null ],
      [ "B4800", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5ab9f410e305b48bc80cf0c770726f9750", null ],
      [ "B9600", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a86d7bec7ffa5d95b1b73c494821e567f", null ],
      [ "B19200", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5af975fbd1b044c9494e3b8bc23c746560", null ],
      [ "B38400", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5accba157b6983e9577b802caa11c3f3a5", null ],
      [ "B57600", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a4158fd234b0cacbb768c3fbedaff3ea4", null ],
      [ "B115200", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5ac1f2933dbb39e111bf14fbd10ca9dba2", null ],
      [ "B230400", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a10be2e726f68d1802176cfb95d6dca15", null ],
      [ "B460800", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a75a6d0c71a125648047039e717716fb8", null ],
      [ "B500000", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5abd03495c33c662c74c8a96e5bacca74a", null ],
      [ "B1000000", "temperatura_8h.html#addcd418f1bff081911a8fb38a62286a5a2b24b2e87b348d3d30f992f2fbb93c0a", null ]
    ] ],
    [ "Paritycheck", "temperatura_8h.html#af9cc96ef779e0c92c5da6c7925c7411c", [
      [ "even", "temperatura_8h.html#af9cc96ef779e0c92c5da6c7925c7411ca43c532333cafbed64b6f286a9432a44e", null ],
      [ "odd", "temperatura_8h.html#af9cc96ef779e0c92c5da6c7925c7411ca67372da54dac061cd9d8c3600b2c1e90", null ],
      [ "off", "temperatura_8h.html#af9cc96ef779e0c92c5da6c7925c7411ca53ace14c115e45153a1c9105accceb4c", null ],
      [ "mark", "temperatura_8h.html#af9cc96ef779e0c92c5da6c7925c7411ca1a4cbaae11bc6a17abc635e99435befa", null ]
    ] ],
    [ "Stopbits", "temperatura_8h.html#afb371079af6706a4ccfbec4f4607747c", [
      [ "one", "temperatura_8h.html#afb371079af6706a4ccfbec4f4607747ca470762e93142d967e17ff201a373c133", null ],
      [ "onePointFive", "temperatura_8h.html#afb371079af6706a4ccfbec4f4607747cabf237e5ea440084fd1b436d9b3de9d6a", null ],
      [ "two", "temperatura_8h.html#afb371079af6706a4ccfbec4f4607747cac53cac5d8c690a430b8e873887fa2c26", null ]
    ] ],
    [ "closeSerialPort", "temperatura_8h.html#a39bc1299bbb87502559041161c184671", null ],
    [ "ErrorExit", "temperatura_8h.html#abb2413b22489e04fc11c2daceda3643f", null ],
    [ "openSerialPort", "temperatura_8h.html#afae25e38e5e6a1b050d3f16035d67bcc", null ],
    [ "readFromSerialPort", "temperatura_8h.html#ab2ce3860df78039a57dfdf01e72070df", null ],
    [ "writeToSerialPort", "temperatura_8h.html#ab1a7f6dc42a64f6fd7a1f97bf2a3352f", null ]
];