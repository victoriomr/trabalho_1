var conio_8h =
[
    [ "text_info", "structtext__info.html", "structtext__info" ],
    [ "COLORS", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50", [
      [ "BLACK", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50af77fb67151d0c18d397069ad8c271ba3", null ],
      [ "BLUE", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a35d6719cb4d7577c031b3d79057a1b79", null ],
      [ "GREEN", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50aa60bd322f93178d68184e30e162571ca", null ],
      [ "CYAN", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50aafe71cad474c15ce63b300c470eef8cc", null ],
      [ "RED", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50af80f9a890089d211842d59625e561f88", null ],
      [ "MAGENTA", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a56926c820ad72d0977e7ee44d9916e62", null ],
      [ "BROWN", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a1fa14482e7e4dc1332ab8c9d995fe570", null ],
      [ "LIGHTGRAY", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a3dcbd50f6d434719ddfb9da673977307", null ],
      [ "DARKGRAY", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a52a9af0edd45f66d37996edcb1ca69f0", null ],
      [ "LIGHTBLUE", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a272b71e84bc2640b193e9fe3c72cb3de", null ],
      [ "LIGHTGREEN", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a0c9ed06b5de60ddd26bb2808e5f4b5dd", null ],
      [ "LIGHTCYAN", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50ac8d7b8737ca95d137a05f3bb8f3d1a17", null ],
      [ "LIGHTRED", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50af3a6d81d1da6f2134cc3cca6f02b1114", null ],
      [ "LIGHTMAGENTA", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50ae19b6d1fedca830c608811b77bd0affc", null ],
      [ "YELLOW", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50ae735a848bf82163a19236ead1c3ef2d2", null ],
      [ "WHITE", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a283fc479650da98250635b9c3c0e7e50", null ],
      [ "BLINK", "conio_8h.html#ab983350d6d1201c0ebd709320e7a0d50a43e7c26b8df0104b9f389e32ac3f72c7", null ]
    ] ],
    [ "CURSORTYPE", "conio_8h.html#aead4d13031ece577b58d73a6bb67f98a", [
      [ "_NOCURSOR", "conio_8h.html#aead4d13031ece577b58d73a6bb67f98aa24e230a666f422da19260ba96036d847", null ],
      [ "_SOLIDCURSOR", "conio_8h.html#aead4d13031ece577b58d73a6bb67f98aaa656aa95dcc69f25dea32a55903f1e7e", null ],
      [ "_NORMALCURSOR", "conio_8h.html#aead4d13031ece577b58d73a6bb67f98aaf604caa88a78111c376457d58be6245b", null ]
    ] ],
    [ "c_clrscr", "conio_8h.html#af862ab1a5237bea559fb45150e3bb02c", null ],
    [ "c_getch", "conio_8h.html#af0ab3098a3c2f59c7177a87cc9ada5e4", null ],
    [ "c_getche", "conio_8h.html#a130d763ed6761631eef2419a986b8900", null ],
    [ "c_gettextinfo", "conio_8h.html#a82070a9e2d1675aa25d4e1888511cf0e", null ],
    [ "c_gotoxy", "conio_8h.html#a8c6d192ba8e3426d41d7e80773361c10", null ],
    [ "c_kbhit", "conio_8h.html#af2a9f4517f9baf6ed35b6ccaa2b4f963", null ],
    [ "c_setcursortype", "conio_8h.html#a7ab5941300d106210ea045914023e784", null ],
    [ "c_textattr", "conio_8h.html#a12c7baa6bbc9996f756c2091494c3064", null ],
    [ "c_textbackground", "conio_8h.html#a289e2408acc8be3d71bf197e9427e773", null ],
    [ "c_textcolor", "conio_8h.html#a435a847d85830f231c452a1196b2c35a", null ],
    [ "c_wherex", "conio_8h.html#ace94eb2cc97d16add0efd472fde3bcf9", null ],
    [ "c_wherey", "conio_8h.html#a9bdbacbef7388943ac69875d32fb8b4c", null ]
];