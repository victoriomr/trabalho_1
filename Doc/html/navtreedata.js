/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Trabalho 1 de Victorio", "index.html", [
    [ "Classes", "annotated.html", [
      [ "Lista de Classes", "annotated.html", "annotated_dup" ],
      [ "Índice dos Componentes", "classes.html", null ],
      [ "Hierarquia de Classes", "hierarchy.html", "hierarchy" ],
      [ "Membros de classe", "functions.html", [
        [ "Todos", "functions.html", null ],
        [ "Funções", "functions_func.html", null ],
        [ "Variáveis", "functions_vars.html", null ],
        [ "Funções Relacionadas", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Arquivos", "files.html", [
      [ "Lista de Arquivos", "files.html", "files_dup" ],
      [ "Membros dos Arquivos", "globals.html", [
        [ "Todos", "globals.html", null ],
        [ "Funções", "globals_func.html", null ],
        [ "Enumerações", "globals_enum.html", null ],
        [ "Enumeradores", "globals_eval.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_data_8h.html"
];

var SYNCONMSG = 'clique para desativar a sincronização do painel';
var SYNCOFFMSG = 'clique para ativar a sincronização do painel';