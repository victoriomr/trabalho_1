/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página Principal",url:"index.html"},
{text:"Classes",url:"annotated.html",children:[
{text:"Lista de Classes",url:"annotated.html"},
{text:"Índice dos Componentes",url:"classes.html"},
{text:"Hierarquia de Classes",url:"hierarchy.html"},
{text:"Membros de classe",url:"functions.html",children:[
{text:"Todos",url:"functions.html",children:[
{text:"_",url:"functions.html#index__5F"},
{text:"a",url:"functions.html#index_a"},
{text:"c",url:"functions.html#index_c"},
{text:"d",url:"functions.html#index_d"},
{text:"e",url:"functions.html#index_e"},
{text:"f",url:"functions.html#index_f"},
{text:"g",url:"functions.html#index_g"},
{text:"i",url:"functions.html#index_i"},
{text:"l",url:"functions.html#index_l"},
{text:"m",url:"functions.html#index_m"},
{text:"n",url:"functions.html#index_n"},
{text:"o",url:"functions.html#index_o"},
{text:"p",url:"functions.html#index_p"},
{text:"s",url:"functions.html#index_s"},
{text:"t",url:"functions.html#index_t"},
{text:"v",url:"functions.html#index_v"}]},
{text:"Funções",url:"functions_func.html",children:[
{text:"a",url:"functions_func.html#index_a"},
{text:"c",url:"functions_func.html#index_c"},
{text:"d",url:"functions_func.html#index_d"},
{text:"e",url:"functions_func.html#index_e"},
{text:"f",url:"functions_func.html#index_f"},
{text:"g",url:"functions_func.html#index_g"},
{text:"i",url:"functions_func.html#index_i"},
{text:"l",url:"functions_func.html#index_l"},
{text:"m",url:"functions_func.html#index_m"},
{text:"p",url:"functions_func.html#index_p"},
{text:"s",url:"functions_func.html#index_s"}]},
{text:"Variáveis",url:"functions_vars.html"},
{text:"Funções Relacionadas",url:"functions_rela.html"}]}]},
{text:"Arquivos",url:"files.html",children:[
{text:"Lista de Arquivos",url:"files.html"},
{text:"Membros dos Arquivos",url:"globals.html",children:[
{text:"Todos",url:"globals.html",children:[
{text:"_",url:"globals.html#index__5F"},
{text:"b",url:"globals.html#index_b"},
{text:"c",url:"globals.html#index_c"},
{text:"d",url:"globals.html#index_d"},
{text:"e",url:"globals.html#index_e"},
{text:"g",url:"globals.html#index_g"},
{text:"l",url:"globals.html#index_l"},
{text:"m",url:"globals.html#index_m"},
{text:"o",url:"globals.html#index_o"},
{text:"p",url:"globals.html#index_p"},
{text:"r",url:"globals.html#index_r"},
{text:"s",url:"globals.html#index_s"},
{text:"t",url:"globals.html#index_t"},
{text:"w",url:"globals.html#index_w"},
{text:"y",url:"globals.html#index_y"}]},
{text:"Funções",url:"globals_func.html"},
{text:"Enumerações",url:"globals_enum.html"},
{text:"Enumeradores",url:"globals_eval.html",children:[
{text:"_",url:"globals_eval.html#index__5F"},
{text:"b",url:"globals_eval.html#index_b"},
{text:"c",url:"globals_eval.html#index_c"},
{text:"d",url:"globals_eval.html#index_d"},
{text:"e",url:"globals_eval.html#index_e"},
{text:"g",url:"globals_eval.html#index_g"},
{text:"l",url:"globals_eval.html#index_l"},
{text:"m",url:"globals_eval.html#index_m"},
{text:"o",url:"globals_eval.html#index_o"},
{text:"r",url:"globals_eval.html#index_r"},
{text:"t",url:"globals_eval.html#index_t"},
{text:"w",url:"globals_eval.html#index_w"},
{text:"y",url:"globals_eval.html#index_y"}]}]}]}]}
