/*
 * Data.h
 *
 *  Created on: 20 de set de 2019
 *      Author: Vitorio
 */

#ifndef DATA_H_
#define DATA_H_
#include <iostream>
#include <string>
#include <time.h>
using namespace std;
class DataHora {
	bool bissexto(int ano) {
		return ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0)));
	}

public:
	int _dia, _mes, _ano, _hora, _minuto, _segundo;
	DataHora() { //Inicialmente mostra a data e hora atual
		time_t now = time(0);
		tm *ltm = localtime(&now);
		_dia = ltm->tm_mday ;
		_mes = ltm->tm_mon + 1;
		_ano = ltm->tm_year + 1900;
		_hora = ltm->tm_hour;
		_minuto = ltm->tm_min;
		_segundo = ltm->tm_sec;

	}

	DataHora(int dia, int mes, int ano, int hora, int minuto, int segundo) {
//		setData(dia, mes, ano);
		_dia = dia;
		_mes = mes;
		_ano = ano;
		_hora = hora;
		_minuto = minuto;
		_segundo = segundo;
	}

	int getAno() {
		return _ano;
	}
	;

	int getDia() {
		return _dia;
	}
	;
	int getMes() {
		return _mes;
	}
	;
	bool setDia(int dia) {
		short dias[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (bissexto(_ano))
			dias[1] = 29;
		if ((dia > 0) && (dia <= dias[_mes - 1])) {
			_dia = dia;
			return true;
		} else
			return false;
	}
	;
	bool setMes(int mes) {
		if ((mes > 0) && (mes < 13)) {
			_mes = mes;
			return true;
		} else {
			return false;
		}
	}
	;
	bool setAno(int ano) {
		if (ano > 0) {
			_ano = ano;
			return true;
		} else
			return false;
	}
	;

	void getData() {
		if (_dia < 10)
			cout << "0";
		cout << _dia << "/";
		if (_mes < 10)
			cout << "0";
		cout << _mes << "/" << _ano;
	}

	bool setData(int dia, int mes, int ano) {
		if ((setAno(ano)) && (setMes(mes)) && (setDia(dia))) {
			return true;
		} else {
			return false;
		}

	}

	friend ostream& operator<<(std::ostream& os, const DataHora & dh) {
		os << dh._dia << ";" << dh._mes << ";" << dh._ano << ";" << dh._hora
				<< ";" << dh._minuto << ";" << dh._segundo;
		return os;
	}

};

#endif /* DATA_H_ */
