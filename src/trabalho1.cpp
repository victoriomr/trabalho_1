#include <mutex>
// malibor: mutex needs to be visible from here
std::mutex lista_mutex;

#include "Estado.h"
#include "conio.h"
#include <locale.h> //necess�rio para usar setlocale
#include <thread>
#include <future>
#include <functional>
#include <atomic>

using namespace std;

void LeDados(ListaEncadeada<Estado>& lista, portaSerial* s, DataHora dataHora,
		bool vent, bool res) {
	string linha;
	cout << "\t\t\tDados da execu��o anterior do arquivo:" << endl;
	ifstream Ler("arquivo.txt");
	if (Ler.fail()) {
		cout << "Ocorreu um erro na Leitura do Arquivo" << endl;
	}
	while (!Ler.eof()) {
		getline(Ler, linha);
		Estado e;
		// malibor: forgot to lock here too
		lista_mutex.lock();
		lista.insereF(e);
		lista_mutex.unlock();
		cout << linha << endl;
	}
	Ler.close();
}

// malibor: change to pointer arg, to make async work
void SalvaDados(ListaEncadeada<Estado>* lista) {
	cout << "\t\t\t Criando arquivo de dados txt" << endl;
	ofstream escreve("arquivo.txt");
	if (!escreve.is_open()) {
		cout << "Falha ao abrir o arquivo" << endl;
		return;
	} else {
//		Estado e;
//		e.setS(s);
//		e.setTemperatura();
//		e.setDataHora(dataHora);
//		e.setVent(vent);
//		e.setRes(res);
//		lista.insereF(e);

		// malibor: lista will be accessed in switch loop too, thats why we need to syncronyze
		lista_mutex.lock();
		for (int i = 0; i < lista->tam; ++i)
			escreve << lista->pos(i) << endl;
		lista_mutex.unlock();

		cout << "\t\t\t Arquivo criado com sucesso" << endl;
		escreve.close();
	}

}
void EscreveDadosExcel(ListaEncadeada<Estado>& lista) {
	cout << "Criando Arquivo de Dados csv" << endl;
	ofstream escreve("Dados.csv");
	if (!escreve.is_open()) {
		cout << "Falha ao abrir o arquivo" << endl;
		return;
	} else {
		// malibor: we need to lock here too
		lista_mutex.lock();
		for (int i = 0; i < lista.tam; ++i) {
			escreve << lista.pos(i) << endl;
		}
		lista_mutex.unlock();
		escreve.close();
	}
	cout << "Lista inclu�da com sucesso no arquivo csv " << endl;
}		//fechando escreve dados excel

void LimpaExcel(ListaEncadeada<Estado>& lista) {
	ofstream Delete("Dados.csv");
	// malibor: we need to lock here too
	lista_mutex.lock();
	for (int i = 0; i < lista.tam; ++i) {
		Delete << " ";
	}
	lista_mutex.unlock();
	cout << "Dados deletados com sucesso" << endl;
	Delete.close();
}		//fechando limpaexcel

int main() {
	setlocale(LC_ALL, "Portuguese");
	int n = 2, operacao; //Porta serial inicial e Valor do Menu;
	float qt_leituras, b;

	// malibor: shared trivial variable needs atomic access
	double intervalo = 2;

	ListaEncadeada<Estado> lista;
	Interface _interface(n);
	c_clrscr();
	// malibor: we only need to call async version
	//_interface.mostraTemperatura(intervalo);
	// malibor: we need to take address of intervalo, because it will change in switch
	async(&Interface::mostraTemperatura, &_interface, &intervalo);
	// malibor: taking address instead of reference
	async(SalvaDados, &lista);
//	thread first{mostraTemperatura, intervalo};//Execu��o das threads
//	first.join();
	while (true) { //Menu e Programa Principal
		c_textcolor(BLACK);
		c_textbackground(WHITE);
		cout << "\t\t\tMenu Principal" << endl;
		cout << "\t0\tLer a temperatura e armazenar na lista encadeada" << endl;
		cout << "\t1\tSalvar dados em txt" << endl;
		cout << "\t2\tAjustar intervalo de exibi��o da temperatura" << endl;
		cout << "\t3\tAjustar limiar de temperatura" << endl;
		cout << "\t4\tLeituras no formato datalog" << endl;
		cout << "\t5\tExportar os dados para o Excel" << endl;
		cout << "\t6\tLimpar todos os dados" << endl;
		cout << "\t7\tLevantamento de ocorr�ncias de uma data" << endl;
		cout << "\t8\tC�lculo de valores estat�sticos" << endl;
		cout << "\t9\tExportar dados no formato de gr�fico" << endl;
		cout << "\t10\tEscolher a porta serial da comunica��o" << endl;
		cout << "\t11\tSair" << endl;
		cout << "Digite uma op��o do menu: " << endl;

		cin >> operacao;

		switch (operacao) {
		case 0: {
			c_clrscr();
			Estado _estado = _interface.getEstado();
			// malibor: lista will be accessed in SalvaDados too, thats why we need to syncronyze
			lista_mutex.lock();
			lista.insereF(_estado);
			lista_mutex.unlock();
			break;
		}
		case 1: {
			c_clrscr();
			// malibor: now working with pointer
			SalvaDados(&lista);
			break;
		}
		case 2: {
			c_clrscr();
			cout
					<< "Entre o intervalo de tempo em que a temperatura � mostrada: "
					<< endl;
			// malibor: lock here..
			_interface.intervalo_mutex.lock();
			cin >> intervalo;
			_interface.intervalo_mutex.unlock();
			break;
		}
		case 3: {
			c_clrscr();
			_interface.setLimites();
			_interface.Controle();
			break;
		}
		case 4: {
			c_clrscr();
			cout << "Digite o n�mero de leituras desejado:";
			cin >> qt_leituras;
			cout << endl << "Escolha o tempo entre cada leitura: ";
			cin >> b;
			//	Datalog g(qt_leituras, b, &s);
			break;
		}
		case 5: {
			c_clrscr();
			EscreveDadosExcel(lista);
			break;
		}
		case 6: {
			c_clrscr();
			LimpaExcel(lista);
			break;
		}
//	case 7:
//		c_clrscr();
			//break;
//	case 8:
//		c_clrscr();
			//break;
//	case 9:
//		c_clrscr();
			//break;
		case 10: {
			c_clrscr();
			cout << "Digite o n�mero da porta serial: " << endl;
			cin >> n;
			_interface.setPorta(n);
			break;
		}
		case 11: {
			c_clrscr();
			// malibor: now working with pointer
			SalvaDados(&lista);
			exit(0);
			break;
		}
			c_clrscr();
			cout << "\nAperte qualquer tecla, para voltar ao menu" << endl;

		}
	}

	return 0;
}
