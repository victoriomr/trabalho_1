/**
 * Classe Estado, uma classe para armazenamento dos estados da ventoinha, do resistor, do valor de temperatura medido atrav�s da porta serial, assim como a data e o tempo.
 */
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <string>
#include "Data.h"
#include "Hora.h"
#include <windows.h>
#include "temperatura.h"
#include <fstream>
#include <ctime>
#include <stdio.h>
#include "ListaEncadeada.h"
#include "ListaEncadeada.cpp"
#include <mmsystem.h>
#include "conio.h"
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <atomic>

using namespace std;
class Estado {
public:
	float _temperatura;/*!< _temperatura atributo para armazenar a temperatura do diodo. */
	bool _vent, _res;/*!< _res, _vent atributos para armazenar os estados da ventoinha e resistor, ligado ou desligado. */
	DataHora _dataHora; /*!< _dataHora atributo para armazenar o dia e horade cada objeto da classe data */

	/*!
	 Método construtor para armazenamennto de estados iniciais de temperatura, temperatura m�nima, m�xima, data e hora, estado do resistor e da ventoinha e da porta serial.

	 Este método chama os métodos setdataHora, setTemperatura, setS, setRes, setVent.
	 */
	Estado(float temperatura, DataHora dataHora, bool vent, bool res) {
		_temperatura = temperatura;
		_vent = vent;
		_res = res;
		_dataHora = dataHora;
	}
	/*!
	 Método construtor para armazenamennto de _vent e _res inicialmente desligados*/
	Estado() {    //Define estado inicial
	}
	/*!

	 */
	friend std::ostream& operator<<(std::ostream& os, const Estado &e) {
		os << e._dataHora << ";" << e._temperatura << ";" << e._res << ";"
				<< e._vent;
		return os;
	}

	const DataHora& getDataHora() const {
		return _dataHora;
	}

	bool getRes() const {
		return _res;
	}

	float getTemperatura() const {
		return _temperatura;
	}

	bool getVent() const {
		return _vent;
	}
};

class Interface: public Estado {
	portaSerial _s;
	float _tmin, _tmax;

public:

	// malibor: mutex needs to be here!
	// update: missing std:: header does not use namespace std!
	std::mutex intervalo_mutex;

	Interface(int porta) {
		// Cria a classe porta serial
		_s = portaSerial();
		_s.abrir(porta);
		_vent = false;
		_res = false;
		_tmin = 20;
		_tmax = 25;
	}

	void setVent(bool valor) {
		_s.enviaSerial(valor ? 3 : 5);
		_vent = valor;
	}

	void setRes(bool valor) {
		_s.enviaSerial(valor ? 2 : 4);
		_res = valor;
	}

	Estado getEstado() {
		string _leitura = _s.enviaSerial(0);
		replace(_leitura.begin(), _leitura.end(), '.', ',');
		_temperatura = atof(_leitura.c_str());
		cout << _temperatura << endl;
		Controle();
		return Estado(_temperatura, _dataHora, _vent, _res);
	}

	// malibor: variable type needs to be consistant across calls, eitehr double or float ( choosed double )
	// malibor: we need to take address of intervalo, because it will change in switch
	void mostraTemperatura(double* intervalo) {
		string _leitura = _s.enviaSerial(0);
		replace(_leitura.begin(), _leitura.end(), '.', ',');
		_temperatura = atof(_leitura.c_str());
		while (true) {
			cout << _temperatura << endl;
			// malibor: locking here too! note that we lock only around value!
			// not for the duration of sleep, that would be wrong!
			intervalo_mutex.lock();
			auto temp = *intervalo;
			intervalo_mutex.unlock();

			Sleep(temp * 1000);
		}
	}

	void Controle() {
		if (_temperatura > _tmax) {
			setVent(true);
			setRes(false);
			c_textcolor(RED);
			cout << "Ventoinha ligada" << endl;
			PlaySound(TEXT("audio.wav"), NULL, SND_FILENAME);

		} else {
			if (_temperatura < _tmin) {
				setVent(false);
				setRes(true);
				cout << "Resistor ligado" << endl;
			} else {
				setVent(false);
				setRes(false);
				cout << "Resistor e Ventoinha desligados" << endl;
			}
		}
	}

	void ExibeTemperatura() {
		cout << "A temperatura atual em �C �: " << _temperatura << endl;
	}

	void setLimites() {
		while (true) {
			cout << "Temperatura m�xima do diodo: " << endl;
			cin >> _tmax;
			cout << "Temperatura m�nima do diodo: " << endl;
			cin >> _tmin;
			if (_tmax < _tmin)
				cout << "Atemperatura m�xima n�o pode ser menor que a m�nima"
						<< endl;
			else if (_tmax == _tmin)
				cout << "As temperaturas m�xima e m�nima n�o podem ser iguais."
						<< endl;
			else
				break;
		}
	}

	void setPorta(int porta) {
		_s.fechar();
		_s.abrir(porta);
	}

};

class Datalog {
public:
	Datalog(int qt_leituras, int b) { //M�todo para exibi��o constante da temperatura na tela
		//	s = enderecoDeS;

		// malibor: qt_leituras need to be constant, as a workaround using dynamic memory, deleting memory at the end of constructor
		float* v = new float[qt_leituras];

		float /*v[qt_leituras],*/ media = 0, min = 100, max = 0, mediana = 0;
		ofstream DadosSalvos("datalog.txt");
		if (!DadosSalvos.is_open()) {
			cout << "Falha ao abrir o arquivo" << endl;
			return;
		} else {
			for (int i = 0; i < qt_leituras; i++) {
				//	v[i] = getTemperatura();
				media += v[i] / qt_leituras; //C�lculo da m�dia dos valores de temperaturas exibidos
				if (max < v[i])
					max = v[i];
				if (min > v[i])
					min = v[i];

				Sleep(b * 1000);
			}
			DadosSalvos << "Temperaturas no formato datalog: " << endl;
			for (int i = 0; i < qt_leituras; i++) { //Ordena��o do vetor
				for (int j = i + 1; j < qt_leituras; j++) {
					if (v[i] > v[j])
						swap(v[i], v[j]);
				}

				DadosSalvos << v[i] << endl;

			}

			if (qt_leituras % 2 != 0)
				mediana = v[((qt_leituras / 2))];
			if (qt_leituras % 2 == 0)
				mediana = (v[qt_leituras / 2] + v[(qt_leituras / 2) + 1]) / 2;

			cout << " A temperatura m�xima em �C registrada foi: "
					<< v[qt_leituras - 1] << endl;
			cout << " A temperatura m�nima �C registrada foi: " << v[0] << endl;
			cout << " A medida de dispers�o mediana �: " << mediana << endl;
			cout << " A medida de dispers�o m�dia �: " << media << endl;
		}
		DadosSalvos.close();

		// malibor: deleting here
		delete[] v;
	}
};
