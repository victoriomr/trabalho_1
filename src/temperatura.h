#ifndef TEMPERATURA_H_
#define TEMPERATURA_H_
#include <cstdio>
#include <iostream>
#include <string>
#include <windows.h>
using namespace std;


enum Baudrate
{
	B50	= 50,
	B110	= 110,
	B150	= 150,
	B300	= 300,
	B1200	= 1200,
	B2400	= 2400,
	B4800	= 4800,
	B9600   = 9600,
	B19200	= 19200,
	B38400	= 38400,
	B57600	= 57600,
	B115200 = 115200,
	B230400	= 230400,
	B460800	= 460800,
	B500000 = 500000,
	B1000000= 1000000
};

enum Stopbits
{
	one = ONESTOPBIT,
	onePointFive = ONE5STOPBITS,
	two = TWOSTOPBITS
};

enum Paritycheck
{
	even = EVENPARITY,
	odd  = ODDPARITY,
	off  = NOPARITY,
	mark = MARKPARITY
};

// malibor: change LPTSTR to LPCTSTR because args in call site are const
void ErrorExit(LPCTSTR lpszFunction);


/**
	\brief Opens a new connection to a serial port
	\param portname		name of the serial port(COM1 - COM9 or \\\\.\\COM1-COM256)
	\param baudrate		the baudrate of this port (for example 9600)
	\param stopbits		th nuber of stoppbits (one, onePointFive or two)
	\param parity		the parity (even, odd, off or mark)
	\return			HANDLE to the serial port
	*/
HANDLE openSerialPort(LPCSTR portname,enum Baudrate baudrate, enum Stopbits stopbits, enum Paritycheck parity);

/**
	\brief Read data from the serial port
	\param hSerial		File HANDLE to the serial port
	\param buffer		pointer to the area where the read data will be written
	\param buffersize	maximal size of the buffer area
	\return				amount of data that was read
	*/
DWORD readFromSerialPort(HANDLE hSerial, char * buffer, int buffersize);
/**
	\brief write data to the serial port
	\param hSerial	File HANDLE to the serial port
	\param buffer	pointer to the area where the read data will be read
	\param length	amount of data to be read
	\return			amount of data that was written
	*/
DWORD writeToSerialPort(HANDLE hSerial, char * data, int length);

void closeSerialPort(HANDLE hSerial);


class portaSerial{
HANDLE h;  

public:
  //Abre a porta serial n�mero N
  void abrir(int porta);
  // Encerra uma conex�o
  void fechar();
  // Envia um comando pela porta seria e recebe a reposta como String
  string enviaSerial(int n);
};

#endif
